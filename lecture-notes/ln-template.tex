%% Compile this file per
%%
%%     pdflatex -shell-escape ln-template
%%
%% if you modify the gnuplot examples. Otherwise pdflatex alone should be sufficient.

\documentclass[a4paper]{tufte-handout}

\usepackage[utf8]{inputenx}

\usepackage{xcolor,tikz,pgfplots,amsmath,amssymb,mathtools,graphicx,listings}
%% It is useful to lookup the documentation for these packages to
%% learn about all the many possibilities they provide, e.g., use
%%
%%     texdoc pgfplots
%%
%%  or
%%
%%     texdoc listings

%% set up the listings package for highlighting BASH code
\lstset{language=bash, backgroundcolor=\color{blue!10}, basicstyle=\ttfamily\small}

%% set up tikz package
%\tikzset{compat=1.}
\usetikzlibrary{intersections}

%% set up pgfplots package
\pgfplotsset{
  % width=5cm,
  width=0.45\linewidth,
  compat=1.16}

\DeclareMathOperator{\myfunc}{my\,func}

\begin{document}

%% create title
\title[short title]{Lecture Notes Example}
\author{My name here}
\date{2018/10/06}
\maketitle{}

\begin{abstract}
  The Tufte classes are great for lecture notes. Their design
  resembles that of undergraduate textbooks with a wide margin that
  can accommodate figures and tables.
\end{abstract}

% \clearpage % start a new page
\tableofcontents{}

% \clearpage
\section{Package documentation}
\label{sec:documentation}

More detailed examples and documentation specifically for the
\lstinline{tufte-handout} document class is shipped with your \LaTeX\
installation. Access the documentation from the command line with
\begin{lstlisting}
  texdoc sample-handout
\end{lstlisting}

\section{Figures}
\label{sec:figures}

\begin{marginfigure}[-15em]
  \includegraphics[width=\linewidth]{ghsystem_explos.pdf} % the files ghsystem_explos.pdf should come with your TeX installation 
  \caption{You can include external figures. Vector graphics in PDF
    format is preferrable for line art, but you can also include JPG,
    PNG and a few other formats.}
\end{marginfigure}

The best typesetting quality is achieved by creating figures directly
with \LaTeX. Notice how in Figure~\ref{fig:tikz-example} the font
within the figure matches the font in the main text. This is desirable
and usually not the case when figures are included that were generated
with external programs.

\begin{figure}
  \begin{tikzpicture}[scale=3]
    \clip (-2,-0.4) rectangle (2,1.2);
    \draw[step=.5cm,gray,very thin] (-1.4,-1.4) grid (1.4,1.4);
    \filldraw[fill=green!20,draw=green!50!black] (0,0)
    -- (3mm,0mm) arc [start angle=0, end angle=30, radius=3mm]
    -- cycle;
    \draw[->] (-1.5,0) -- (1.5,0) coordinate (x axis);
    \draw[->] (0,-1.5) -- (0,1.5) coordinate (y axis);
    \draw (0,0) circle [radius=1cm];
    \draw[very thick,red] (30:1cm) -- node[left=1pt,fill=white] {$\sin \alpha$} (30:1cm |- x axis);
    \draw[very thick,blue] (30:1cm |- x axis) -- node[below=2pt,fill=white] {$\cos \alpha$} (0,0);
    \path [name path=upward line] (1,0) -- (1,1);
    \path [name path=sloped line] (0,0) -- (30:1.5cm);
    \draw [name intersections={of=upward line and sloped line, by=t}]
    [very thick,orange] (1,0) -- node [right=1pt,fill=white] {$\displaystyle \tan \alpha \color{black}=
      \frac{{\color{red}\sin \alpha}}{\color{blue}\cos \alpha}$} (t);
    \draw (0,0) -- (t);
    \foreach \x/\xtext in {-1, -0.5/-\frac{1}{2}, 1}
    \draw (\x cm,1pt) -- (\x cm,-1pt) node[anchor=north,fill=white] {$\xtext$};
    \foreach \y/\ytext in {-1, -0.5/-\frac{1}{2}, 0.5/\frac{1}{2}, 1}
    \draw (1pt,\y cm) -- (-1pt,\y cm) node[anchor=east,fill=white] {$\ytext$};
  \end{tikzpicture}
  \caption{Figures in the main body of the text look like this. This
    one is borrowed from \lstinline{pgfmanual.pdf}, the documentation
    of the \lstinline{tikz} package, which you can look at with
    \lstinline|texdoc pgfmanual|.}
  \label{fig:tikz-example}
\end{figure}

If you need to plot data, i.e., graphs of functions in the widest
sense, then the \lstinline|pgfplots| package is the gold standard. It
is able to plot data generated with other programs, e.g., simulation
results, but it is also able to plot some elementary mathematical
functions directly.

  
\begin{figure*}
  \centering
  \begin{tikzpicture}
    \begin{axis}[
      % width=0.45\linewidth,
      title={$f(x,y) = x \exp(-x^{2}-y^{2})$},
      xlabel=$x$, ylabel=$y$,
      small,
      grid=major,
      ]
      \addplot3 [
      surf,
      domain=-2:2,
      domain y=-1.3:1.3,
      ] {exp(-x^2-y^2)*x};
    \end{axis}
  \end{tikzpicture}
  \hfil
  \begin{tikzpicture}
    \begin{axis}[
      % width=0.45\linewidth,
      title={Level curves and gradient field.},
      domain=-2:2,
      view={0}{90},
      axis background/.style={fill=white},
      xlabel=$x$, ylabel=$y$,
      ]
      \addplot3 [
      contour gnuplot={number=9,labels=false},thick, % this one only works if GNUPLOT is installed
      ] {exp(0-x^2-y^2)*x};
      \addplot3 [
      blue,-stealth,samples=15,
      quiver={
        u={exp(0-x^2-y^2)*(1-2*x^2)},
        v={exp(0-x^2-y^2)*(-2*x*y)},
        scale arrows=0.3,
      },
      ] {exp(0-x^2-y^2)*x};
    \end{axis}
  \end{tikzpicture}


  \caption{The \lstinline|pgfplots| package lets you create graphs
    of functions within \LaTeX\ or using \lstinline{gnuplot}.}
  \label{fig:pgfplots}
\end{figure*}

\section{Formul\ae and mathematical style}
\label{sec:formulae}

Formulas (or formul\ae\ if you want to be fancy) are \LaTeX's
strongsuit. Nothing else comes even close.

Text formulas are those that appear within a paragraph of text, like
$\cos^{2} \varphi + \sin^{2} \varphi \equiv 1$. Display formulas are
those that stand out from the text, and they can be numbered or not
numbered like the following.

\paragraph{The standard display formula format} is the most common
one. If you are not referencing a formula, you shouldn't number it.
\[\cos^{2} \varphi + \sin^{2} \varphi \equiv 1\]

%% $$\cos^{2} \varphi + \sin^{2} \varphi \equiv 1$$ is also valid, but discouraged

\paragraph{Numbered and aligned formulas} are important, too. For example, if
\begin{align}
  \label{eq:1}
  \cos^{2} \varphi + \sin^{2} \varphi & = 1\\
  \label{eq:2}
                                      & = \exp 0 = e^{0}  
\end{align}
then we can refer to \eqref{eq:1} and \eqref{eq:2} indidually, or
jointly to \eqref{eq:1}--\eqref{eq:2}. Notice the long hyphen between
the formula numbers. There are differences between `-', `--', and
`---'. Only the middle one is used for anything that looks like a
range, e.g., page ranges or ranges of formula numbers. If you want to
learn more about good style, then
Strunk\&White\cite{strunkwhite1999-the-elements-of-style} is a tiny
yet tremendeously useful investment and read.

Another example of good versus bad style is the placement of
punctuation. There is \emph{never, ever} a space in front of a
punctuation sign---not even if the punctuation is a hyphen. You also
should never use \underline{underlines} and use \emph{emphasis} and
\textbf{bold} text sparingly. And---by the way---\emph{emphasis},
\textit{italics}, and \textsl{slanted text} are not the same, at least
in some of the standard fonts that come with \LaTeX. 

\paragraph{Operator names} are often used improperly by the beginning
typesetter. For example,
\[\max \big\{ \sin(x)\colon x\in\mathbb{R} \big\}\]
looks a lot nicer than
\[max \big\{ \sin(x)\colon x\in\mathbb{R} \big\}.\]

The difference is that in the first instance
\lstinline[language=TeX]-\max- is an operator, and in the second one
\lstinline[language=TeX]-max- (without the backslash) is not. By
convention $max$ does not mean maximum, but in fact
$m\times a\times x$ or $m\cdot a\cdot x$.  So how do you typeset
$\myfunc(x)$ properly? Clearly, 
\begin{equation}
  \label{eq:3}
  \myfunc(x)\ne my func(x).
\end{equation}
The answer is to define your own operator via
\begin{lstlisting}[language=TeX]
\DeclareMathOperator{\myfunc}{my\,func}
\end{lstlisting}
in the preable of the \LaTeX\ document.

Lastly, notice how the formulas above are part of the surrounding
sentences, including punctuation. The sentence around~\eqref{eq:3} is
perhaps a bit minimalistic, but at least the formula does not stand
alone by itself.

\section{References}
\label{sec:references}

\marginnote[1em]{Tip: Start maintaining \emph{one} central file with all
  your references. Adopt a uniform citation key format, like
  \texttt{author2018a} and stick to it. It'll make things easier long
  term, trust me.}%
%%
Typesetting references in lecture notes is not that common. In-line
references may be more appropriate than end-of-chapter or
end-of-document lists of references. In either of these cases,
Bib\TeX\ (or its successors, the biblatex/biber combination) is still
the best way to manage references.
%%

\bibliographystyle{alpha}
\nobibliography{references} % you CAN use absolute paths to a central bibliography file here
\end{document}






%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% mode: latex-math
%%% mode: outline-minor
%%% mode: TeX-source-correlate
%%% TeX-command-extra-options: "-shell-escape"
%%% ispell-local-dictionary: "british"
%%% TeX-command-default: "LaTeX"
%%% TeX-PDF-mode: t
%%% TeX-engine: default
%%% TeX-master: t
%%% End:


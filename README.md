# LaTeX Templates

##### LaTeX templates for articles, presentations, and lecture notes aimed at mathematical typesetting. 

The advise provided in the templates is complementary to each other,
so it may be worthwhile to look at the lecture notes template even if
you plan to write a research paper.

## Writing research articles

The template provided here aims to be complementary to the many
introductions to LaTeX, so it does not provide an introduction to
LaTeX. The intention is to give a little guidance for good style as well
as point to some tools and best practices for scientific writing.

The compiled template looks like
[this](https://bitbucket.org/bjoseru/latex-templates/raw/pdfs/research-paper/paper-template.pdf).

## Presentations

Likewise, the presentation template aims more at stylistic advise (and
sets a negative example itself) than on LaTeX technicalities.

The compiled template looks like
[this](https://bitbucket.org/bjoseru/latex-templates/raw/pdfs/presentation/presentation-template.pdf).


## Lecture notes

Most lecture notes that I have come across are typeset like research
reports or not-too-fancy books. Here is a simple example of a document
class inspired by the typography of Edward Tufte. It should come
shipped along with every modern LaTeX distribution, and its style is
closer to that of the big-name-publisher undergraduate textbooks,
i.e., with a wide margin and lots of potential for margin notes.

The compiled template looks like
[this](https://bitbucket.org/bjoseru/latex-templates/raw/pdfs/lecture-notes/ln-template.pdf).

# How to use the templates

## Method 1, recommended ##

Install [git](https://git-scm.com/) and clone this repository to your hard drive.

## Method 2, for those resistent to good advice ##

You can download the latest snapshot of this respository as

* [zip file](https://bitbucket.org/bjoseru/latex-templates/get/master.zip)
* [tar file, gzip compressed](https://bitbucket.org/bjoseru/latex-templates/get/master.tar.gz)
* [tar file, bzip2 compressed](https://bitbucket.org/bjoseru/latex-templates/get/master.tar.bz2)

# Support

I do not provide any support, however, if you find mistakes or want to
contribute in any way, please feel free to send a pull request through
bitbucket.

If you do have questions or run into any issues, use Google, or forums
like [TeX.SE](https://tex.stackexchange.com/). More than likely,
you'll find all answers there.


# Terms of use

The templates are released under the [MIT license](LICENSE.md), which
basically allows you to use them in any way you want.

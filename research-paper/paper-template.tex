\documentclass[a4paper,twocolumn,DIV20]{scrartcl}

\usepackage[utf8]{inputenx}
\usepackage[OT1]{fontenc}
\usepackage{xcolor,tikz,pgfplots,amsmath,amssymb,mathtools,graphicx,listings,hyperref}

\lstset{language=TeX,basicstyle=\ttfamily\small,extendedchars=true}

\begin{document}
\title{A sample research paper}
\author{My name goes here}
\date{2018/10/06}
\maketitle{}
\begin{abstract}
  \noindent \small
  \textbf{Abstract.}
  An abstract should summarise the work in about 150 words, for
  example: This paper gives advise on how to write a
  mathematical/engineering research paper, while giving appropriate
  pointers to more in-depth literature on subjects of style,
  typesetting, and version control. More typesetting advise can be found
  in the companion templates, in particular, we have tried to avoid
  repetition between these.
\end{abstract}

\section{Introduction}
\label{sec:introduction}

Writing research papers is a common problem for new graduate
students. Especially students in the technical and mathematical
sciences are commonly not sufficiently prepared to write well. While
lack of prior experience with the \LaTeX\ typesetting system is often
seen as the major obstacle by the students, the main problem extends
in fact to lack of understanding of publishing culture and good
writing style.

The exists a plethora of suitable books on typesetting in \LaTeX,
e.g., \cite{griffiths-higham2016:learning-latex},
\cite{kopka-daly2003:guide-to-latex}, and most notably
\cite{mittelbach-goossens-et-al-2004:latex-companion}. Standard
references for stylistic advise include
\cite{chicago-manual-of-style,strunkwhite1999-the-elements-of-style}. Package
documentation for the various \LaTeX\ packages (e.g.,
\lstinline-texdoc pgflots-, \lstinline-texdoc listings-, etc.\ from
the UNIX command line) or the ``AMS Author Handbook Journal Classes''
(\lstinline-texdoc ams-) are also useful and the most current
references.

In this paper we give a brief overview of the typesetting style issues
and the publishing standards that are often expected from authors by
the mathematical and engineering journals they submit their work to.

We do this by writing a template document in \LaTeX, namely the
present manuscript. The text in this document shall be the advise we
are providing, while the source code gives at least some hints on how
the \LaTeX\ typesetting side can be tackled.

This document is organised as follows. In the next section one would
often introduce notation and basic definitions, but we set out the
common structure used in many research papers. In
Section~\ref{sec:how-write-story} we discuss the tense and perspective
of writing. In Section~\ref{sec:style} we give some minimal hints on
style. The pitfalls of referencing external work correctly is
showcased in Section~\ref{sec:correct-referencing}, while more
technical advise is provided in
Section~\ref{sec:technicalities}. Section~\ref{sec:conclusions}
concludes this paper.

\section{The structure of a research paper}
\label{sec:struct-rese-paper}

There is always an introduction and most often a conclusion
section. Abstract and conclusions are similar, but the former is to be
appreciated before the reader has actually read the manuscript, and
the latter summarises the main points or issues for the reader who has
that task already behind them.

The introduction of papers in many engineering and mathematics
journals follows a generic structure, consisting of five paragraphs,
which address the following questions:
\begin{enumerate}
\item What is the problem?
\item What is known about it already?
\item What are we doing in this paper?
\item How are we doing it?
\item How is this paper organised?
\end{enumerate}
The attentive reader will observe that the structure of
Section~\ref{sec:introduction} follows this very outline.

The problem should generally be motivated and put into context. Part
of describing the problem is also to explain why it is or should be of
interest to the research community or the reader in particular.

A crucial part of scientific discourse is to frame your own
contribution in the relevant literature. List or discuss other
approaches that have been attempted, generalisations of the underlying
problem in different but related directions than the presently pursued
one. Give credit and, in case of doubt, be more inclusive than too
strict about whom to cite. After all, chances are that the referees of your
paper may be among those people you cite, or don't cite.

Then you should give a birds-eye perspective of the contribution in
the present manuscript. Don't be technical, but describe in general
terms what is happening, and possibly also why and where the new
approach differs from existing one, in case that might be considered
an issue.

In the next paragraph you should get a bit more technical, use more of
the appropriate jargon and describe the important steps underlying
your contribution.

Lastly, similar to a table of contents, but in text form, you outline
the paper.

\section{How to write a story}
\label{sec:how-write-story}

It may seem obvious, but key to great writing is to indeed tell a
story. A story should be interesting for the reader.

It is usually not of interest to the reader how you came to figure out
a result or made a discovery. That anecdote you can tell in your
memoirs, or during the chat in the coffee break after you have
presented the results in your paper at a conference. The central
figure in the story of your research paper should be the problem you are
writing about.

You should avoid colloquialisms and too repetitive expressions like
``and then \ldots, and then \ldots, and hence \ldots, and then \ldots,
therefore \ldots'' It is much nicer if you can tell a proper story,
but this requires practice. So be prepared that your adviser may ask
you to rewrite your story several times. This is in your best interest
if you want to become a great science writer.

It is good practise to find leading authors in your area who publish
in the journals you want to publish in and study closely not only
their results, but also their style. Often it will not be perfect, but
there will usually be something about it that sells well. And it is
good for you if you figure out what that something is.

\section{Style}
\label{sec:style}

Mathematical papers are commonly written in the first person plural
and in present tense. That applies even if we are a single author.
If this was a research paper, we would have avoided to directly
address the reader (you).

Style is about writing concisely rather than verbosely, about avoiding
long and never-ending sentences with many parenthetic phrases, about
placing commas and related issues. We strongly recommend adding the
little book~\cite{strunkwhite1999-the-elements-of-style} to your
must-read list. Ideally, it is read once a year.

Lastly, always run a spell-checker over your tex-document.

\section{Correct referencing}
\label{sec:correct-referencing}

Correct referencing is about using the proper citation style for the
journal at hand, placing citations in the text correctly, but there is
more. The formatting and correct typing of the references is often
gravely neglected. This can be very off-putting for those references
who happen to be your referees. It also immediately sends the signal
that the author of a paper is sloppy and negligent when already the
references contain grave mistakes.

So how can these mistakes be avoided? The best strategy is to use and
maintain a single bibliographic database for everything you
write. Whenever you correct a mistake in this one central place, it
will trickle down to all the other places where the mistake would have
materialised.

To this end Bib\TeX\ is a blessing. Maintain a central bib-file and
put it under version control, while keeping backups elsewhere. Good
places to get mostly correct Bib\TeX\ entries from include MathSciNet
and ZentralBlatt. IEEExplore on the other hand is notorious for
providing extremely poorly populated Bib\TeX\ entries. These
\emph{always} need manual editing. For example, IEEExplore lets you
download the following ``citation'' in Bib\TeX\ format.

\begin{lstlisting}[escapechar=|]
@INPROCEEDINGS{6760604, 
author={A. Rantzer and
    B. S. R|{\"u}|ffer and G. Dirr},
booktitle={52nd IEEE Conference on
    Decision and Control},
title={Separable Lyapunov
functions for monotone systems},
year={2013}, 
volume={}, 
number={}, 
pages={4590-4594}, 
keywords={...}, 
doi={10.1109/CDC.2013.6760604}, 
ISSN={0191-2216}, 
month={Dec},}
\end{lstlisting}

You may wonder what is wrong with this entry. It does seem to get the
umlaut in the second author name right, which is not always the
case. Be careful with any names that contain or may contain accents or
are transcribed from different alphabets like Cyrillic.

In this case it is the title that is wrong and the page range is at
least poorly formatted. If this citation is used as-is in your
bib-file, the title will be typeset as ``Separable lyapunov functions
for monotone systems'' and the page range as ``4590-4594''. Notice how
lyapunov is suddenly lower rather than upper case. This has to do with
the freedom the bibliography style exercises when it translates
bib-entries to entries in your bibliography. You cannot know in
advance what this translation will do, and you can safely assume that
making everything lower case is a very common feature.

You may be aware that Lyapunov is the name of a person, so Lyapunov
should never be typeset in lower case. Likewise, a page range is a
range, so it should be typeset as such, namely 4590--4594, which is
not the same as 4590-4594.

In the Bib\TeX\ entry, changing two the lines
\begin{lstlisting}
title={Separable {L}yapunov
   functions for monotone systems},
\end{lstlisting}
and
\begin{lstlisting}
pages={4590--4594}, 
\end{lstlisting}
achieves the desired outcome.

There were also times when IEEExplore would report journal names as
something like
\begin{lstlisting}
journal={Automatic Control, IEEE
    Transactions on},
\end{lstlisting}
which in fact should be
\begin{lstlisting}
journal={IEEE Transactions on
    Automatic Control},
\end{lstlisting}
or better yet,
\begin{lstlisting}
journal={{IEEE} {T}rans.\
    {A}utomat.\ {C}ontrol},
\end{lstlisting}
as is the de~facto standard~\cite{ams2018:serials-abbreviations}. The
curly braces protect the abbreviation and capital letters from being
lower cased.  If you are citing several papers from the same journal, it
quickly becomes efficient to use Bib\TeX\ macros for the journal
names.

More finely grained issues are to write out some authors' first names
and abbreviate those of others. Abbreviating everyone's first names
can be a safe workaround.

\section{Technicalities}
\label{sec:technicalities}

Writing documents with \LaTeX\ is a lot like programming. \LaTeX\ may
seem like a markup language, but it is in fact more than that. It has
programming capabilities. This implies that wrong input can produce
interesting error messages. And error messages are indeed
important. When you compile your document, always pay attention to the
status report at the end which will either tell you that the document
was produced without error, or that there were errors. And if there
were errors, you should inspect the log file to find them. This is a
lot like debugging. In fact, it is debugging.

It is also useful to know that there is a difference between \TeX,
\LaTeX, and even different versions of \LaTeX. For practical purposes,
you'll only need to concentrate on \LaTeX 2e, the current standard,
but be aware that \LaTeX 3 is on the horizon (and yet it may still
take years until it becomes practically important). Try not to use
obsolete \TeX\ commands that you may find in many well-intended
templates and guidelines. These include to use \lstinline-\textbf{}-
instead of \lstinline-\bf-, among others. Great practice is to just
include the \texttt{nag} package in your preamble, and it will
complain and warn you whenever you do use these obsolete commands.

Another big technical issue is correct spelling. Yes, it is a
technical issue, as spell-checking in \LaTeX\ documents is not part of
\LaTeX\ itself, but instead a feature that is either provided by your
text editor or an external program invoked by the editor.

In terms of editing and platform, we very highly recommend to work on
a UNIX type operating system. This includes GNU/Linux and MacOS among
others. We are not going to explain the many reasons here, but just
state as a fact that the many Microsoft operating systems are not very
useful for many scientific computing, data handling, and also
typesetting tasks, despite their widespread use. For many a switch to
UNIX comes at the expense of a steep learning curve, but please trust
that this initial investment will repay you with substantial interest
in the long run.

In terms of good text editors, you can safely use whichever editor is
shipped with your \LaTeX\ distribution (usually \TeX live or Mik\TeX\
in case of Windows). Emacs has the advantage that it has the
largest and longest-established user community and supports the widest range of
other tasks, apart from editing \LaTeX\ files, than any more modern
competitor. It is available for any platform under the free GNU
licence (GPL).

Yet another great practice is to familiarise yourself with the version
control system git~\cite{git-scm}. The textbook
\cite{chacon2009-pro-git} is freely available and provides a lot of
background, but all you need to know to get started can be picked up
by watching some of the introductory videos on the git web page. Git
has one notable competitor, with abilities paralleling those of git,
named mercurial. However, this one is used less often and if you
understand one, switching to the other is really easy, so you might as
well go mainstream. The version control systems subversion (aka svn)
and cvs are obsolete, and it is not worth your time to learn how to use
them. They have many drawbacks as compared to the other two.

Version control, especially with git (see \texttt{github.com} and
\texttt{bitbucket.com} as de~facto standard platforms for software and
document exchange), is incredibly useful not only for programmers who
write source code in teams, but also for every writer who revises
their \LaTeX\ document repeatedly. It lets you track changes, revert
changes you made long ago, or look at a very finely grained difference
reports between your version of a tex-file and that of your co-author.

\section{Conclusions}
\label{sec:conclusions}

Mastering the typesetting system alone is only the first step. We have
pointed out that successful writing does also depend on telling a good
story and on being a bit perfectionist about your writing style. Some
helpful tools like version control systems have been mentioned. All
this should be only the starting point for you. Don't expect to master
everything at once, or that you need to have mastered everything
before you can start being productive. You are embarking on a
life-long journey here, and hopefully you are having a good start.

\bibliographystyle{plain}
\bibliography{paper}

\end{document}

% LocalWords:  AMS MathSciNet ZentralBlatt IEEExplore MacOS svn cvs
% LocalWords:  github bitbucket
